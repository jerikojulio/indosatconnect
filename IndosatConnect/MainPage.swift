//
//  MainPage.swift
//  IndosatConnect
//
//  Created by Jeriko on 1/31/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

class MainPage: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak  var responseLabel       : UILabel!
    @IBOutlet weak  var latencyLabel        : UILabel!
    @IBOutlet weak  var debugLabel          : UITextView!
    @IBOutlet weak  var textField           : UITextField!
    @IBOutlet weak  var connectionIndicator : UIActivityIndicatorView!
    @IBOutlet weak  var connectButton       : UIButton!
    private static  var count               = 0.0
    private static  var timer               = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tapToResignResponder = UITapGestureRecognizer(target: self, action: #selector(resignTextfieldResponder))
        view.addGestureRecognizer(tapToResignResponder)
        connectionIndicator.isHidden    = true
        responseLabel.text              = ""
        latencyLabel.text               = ""
        debugLabel.text                 = ""
        debugLabel.layer.borderWidth    = 1
        debugLabel.layer.borderColor    = UIColor.gray.cgColor
        connectButton.layer.borderWidth = 1
        connectButton.layer.borderColor = UIColor.gray.cgColor
        connectButton.backgroundColor   = UIColor(red: 175/255, green: 205/255, blue: 255/255, alpha: 1)
        textField.delegate              = self
        //tests
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func resignTextfieldResponder(){
        textField.resignFirstResponder()
        debugLabel.resignFirstResponder()
    }
    
    @IBAction func connect(_ sender: UIButton) {
        if Reachability.isInternetAvailable(){
            timerStart()
            getStatusCode()
        } else {
            latencyLabel.text = "No Connection"
            responseLabel.text = "No Connection"
            recolorText(textfield: latencyLabel, value: "No Connection")
            recolorText(textfield: responseLabel, value: "No Connection")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if Reachability.isInternetAvailable(){
            timerStart()
            getStatusCode()
        } else {
            latencyLabel.text = "No Connection"
            responseLabel.text = "No Connection"
            recolorText(textfield: latencyLabel, value: "No Connection")
            recolorText(textfield: responseLabel, value: "No Connection")
        }
        return true
    }
    
    func timerStart(){
        MainPage.timer.invalidate()
        MainPage.timer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    func timerAction(){
        MainPage.count += 0.001
    }
    
    func getStatusCode(){
        
        if  let requestURL  : NSURL                 = NSURL(string: (textField.text)!) {
            let urlRequest  : NSMutableURLRequest   = NSMutableURLRequest(url: requestURL as URL)
            
            self.responseLabel.text             = ""
            self.debugLabel.text                = ""
            self.latencyLabel.text              = ""
            self.connectionIndicator.isHidden   = false
            self.connectionIndicator.startAnimating()
            
            let session = URLSession.shared
            let task    = session.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in
                let httpResponse    = response as? HTTPURLResponse
                DispatchQueue.global(qos: .userInitiated).async{
                    if let statusCode   = httpResponse?.statusCode{
                        DispatchQueue.main.async{
                            self.updateStatusCode(statusCode: statusCode)
                        }
                    } else {
                        DispatchQueue.main.async{
                            self.updateStatusCode(statusCode: nil)
                        }
                    }
                }
            }
            task.resume()
            
            var url: URL? = nil
            var contents: String? = ""
            
            DispatchQueue.global(qos: .userInitiated).async{
                do {
                    url             = URL(string: (self.textField.text)!)
                    try contents    = String(contentsOf: url!)
                } catch {
                    contents = String(describing: error)
                }
                DispatchQueue.main.async{
                    self.debugLabel.text = contents
                }
            }
        }
    }
    
    func updateStatusCode(statusCode: Int?){
        MainPage.timer.invalidate()
        
        var status      = "No Response"
        
        if  statusCode  != nil{
            status      = String(statusCode!)
            latencyLabel.text  = String(format: "%.3f", MainPage.count) + " s"
        } else {
            latencyLabel.text  = "No Response"
        }
        
        recolorText(textfield: latencyLabel, value: latencyLabel.text!)
        recolorText(textfield: responseLabel, value: status)
        responseLabel.text              = status
        connectionIndicator.stopAnimating()
        connectionIndicator.isHidden    = true
        MainPage.count                  = 0.0
    }
    
    func recolorText(textfield: UILabel, value: String){
        switch value{
        case "No Response", "No Connection" :
            textfield.textColor = UIColor(red: 200/255, green: 50/255, blue: 50/255, alpha: 1)
        default :
            textfield.textColor = UIColor(red: 50/255, green: 200/255, blue: 50/255, alpha: 1)
        }
    }
}
